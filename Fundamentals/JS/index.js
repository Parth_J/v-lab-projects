// ======================interger value======================
// var a:number = 10;
// var b:number = 20;
// var c:number ;
// c = a + b;
// console.log("c = "+c);
// ======================String value======================
// var firstname:string = "Parth";
// var lastname:string = "Jathar";
// var fullname:string = firstname +  " "  + lastname;
// console.log("Full name :  " +fullname )
// ======================array value======================
// var a:number[] = [1,2,3,4];
// var sum:number = 0;
// for(let i=0;i<a.length;i++)
// {
//     sum+=a[i];
// }
// console.log(sum);
// ======================union type======================
// type User={
//     name:string;
//     id:number;
// }
// type Admin = {
//     id:number;
//     name:string;
//     control:number;
// }
// var newuser:User|Admin;
// newuser={name:"Parth" , id:10};
// console.log(newuser);
// newuser={name:"Parthh", id:2 , control:3}
// // console.log(newuser);
// // var newuser:{              //variable level
// //     name:string;
// //     id:number;
// // }
// // newuser={name:"Parth jathar" , id:1};
// // console.log(newuser);
// // ======================enum======================
// enum Month{
//     jan=1,
//     Feb=2,
//     March=3,
// }
// console.log(Month.jan);
// console.log(Month.March);
// // ======================Array Object======================
// var User:{id:number,name:string}[]=[];
// User.push({id:1,name:"Parth"});
// User.push({id:2,name:"Yash"});
// User.push({id:3,name:"Nikita"});
// console.log(User);
// // ======================Functions======================
// var a:number = 10;
// var b:number = 20;
// function add(a:number,b:number):number{
//     return (a+b);
// }
// const num1:HTMLInputElement=<HTMLInputElement>document.getElementById("Num1");
// const num2:HTMLInputElement=<HTMLInputElement>document.getElementById("Num2");
// const result:HTMLInputElement=<HTMLInputElement>document.getElementById("result");
// //var div:HTMLDivElement
// function add()
// {
//     let a:number=parseInt(num1.value);
//     let b:number=parseInt(num2.value);
//     let c = a+b;
//     result.value = c.toString();
// }
var tbarray = [];
const row = document.getElementById("row");
function createArray() {
    tbarray = new Array(parseInt(row.value)).fill(0).map(() => new Array(2).fill(0));
    genratetable();
}
function genratetable() {
    let tb1 = document.getElementById("tb1");
    tb1.innerHTML = " ";
    let tbody = " ";
    for (let i = 0; i < tbarray.length; i++) {
        tbody += `
            <tr>
            <td><input type="text" id=inp${i} value="${tbarray[i][0]}"></td>
            <td><input type="text" id=out${i} value="${tbarray[i][1]}"></td>  
            </tr>
        `;
    }
    tb1.innerHTML = tbody;
}
function updateArray() {
    for (let i = 0; i < tbarray.length; i++) {
        let inp = document.getElementById(`inp${i}`);
        tbarray[i][0] = parseInt(inp.value);
        tbarray[i][1] = Math.pow(parseInt(inp.value), 3);
    }
    genratetable();
}
//# sourceMappingURL=index.js.map